//
//  UIViewController+NavigationBar.swift
//  BisesDemo
//
//  Created by Yunus TEK on 23.12.2018.
//  Copyright © 2018 Yunus TEK. All rights reserved.
//

import UIKit

extension UIViewController {
    func backButton() {
        guard let navController = self.navigationController else { return }
        navController.navigationBar.topItem?.title = " " // x10 character
        navController.navigationBar.tintColor = UIColor.white
    }
    
    func presentTransparentNavigationBar(isHidden: Bool = true) {
        guard let navController = self.navigationController else { return }
        navController.navigationBar.setBackgroundImage(UIImage(), for:UIBarMetrics.default)
        navController.navigationBar.isTranslucent = isHidden
        navController.navigationBar.shadowImage = UIImage()
        navController.setNavigationBarHidden(false, animated:true)
        navController.navigationItem.hidesBackButton = isHidden
        //        self.edgesForExtendedLayout = UIRectEdge.None // Kenarlar
        navController.navigationBar.barTintColor = UIColor.white
		navController.navigationBar.tintColor = UIColor.white
    }
    
}
