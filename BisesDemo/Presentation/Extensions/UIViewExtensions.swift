//
//  UIImageViewExtensions.swift
//  BisesDemo
//
//  Created by Yunus TEK on 2.02.2019.
//  Copyright © 2019 Yunus TEK. All rights reserved.
//

import UIKit

extension UIView {
	public func setRounded() {
		self.layer.cornerRadius = self.frame.size.width / 2
		self.clipsToBounds = true
	}
	
	public func setBorderColor() {
		self.layer.borderWidth = 3
		self.layer.borderColor = UIColor.white.cgColor
	}
}
