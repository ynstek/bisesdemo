//
//  UIViewController+Presenters.swift
//  BisesDemo
//
//  Created by Yunus TEK on 24.12.2018.
//  Copyright © 2018 Yunus TEK. All rights reserved.
//

import UIKit

extension UIViewController {
	func showAlert(_ titleMessage: String?, bodyMessage: String?, yes: @escaping () -> Void, no: (() -> Void)? = nil){
		BisesAlertPresenter().alert(titleMessage, bodyMessage: bodyMessage, yes: {
            yes()
        }) {
			if (no != nil) {
				no!()
			}
        }
    }
    
    func showAlert(_ titleMessage: String, bodyMessage: String, showButton: Bool = true) {
        BisesAlertPresenter().alert(titleMessage, bodyMessage: bodyMessage, showButton: showButton)
    }
    
    func showAlert(_ titleMessage: String, bodyMessage: String, ok: @escaping () -> Void){
        BisesAlertPresenter().alert(titleMessage, bodyMessage: bodyMessage) {
            ok()
        }
    }
    
    func showAlert(_ titleMessage: String, bodyMessage: String, _ switchTitle: String, switchStatus: Bool, success: @escaping (_ status: Bool) -> Void) {
        BisesAlertPresenter().alert(titleMessage, bodyMessage: bodyMessage, switchTitle, switchStatus) { (status) in
            success(status)
        }
    }
    
    func showAlert(title: String, pickerdate: Date, minimumdate: Date?, apply: @escaping (_ date: Date) -> Void, cancel: @escaping () -> Void?) {
        BisesAlertPresenter().alert(title: title, pickerdate: pickerdate, minimumdate: minimumdate, apply: { (date) in
            apply(date)
        }) {
            cancel()
        }
    }
}
