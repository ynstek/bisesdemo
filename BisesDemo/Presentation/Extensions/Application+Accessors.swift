//
//  Application+Accessory.swift
//  BisesDemo
//
//  Created by Yunus TEK on 2.02.2019.
//  Copyright © 2019 Yunus TEK. All rights reserved.
//

import UIKit

extension UIApplication {
	func topMostViewController() -> UIViewController? {
		return self.keyWindow?.rootViewController!.topMostViewController()
	}
}
