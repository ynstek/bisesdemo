//
//  UIViewController+Accessors.swift
//  BisesDemo
//
//  Created by Yunus TEK on 2.02.2019.
//  Copyright © 2019 Yunus TEK. All rights reserved.
//

import UIKit

extension UIViewController {
	
	func firstApplicableViewController() -> UIViewController? {
		if (self is UITabBarController) {
			let tabBarController = self as? UITabBarController
			return tabBarController?.selectedViewController?.firstApplicableViewController()
		} else if (self is UINavigationController) {
			let navigationController = self as? UINavigationController
			return navigationController?.visibleViewController?.firstApplicableViewController()
		} else if self.presentedViewController != nil {
			let presentedViewController: UIViewController = self.presentedViewController!
			return presentedViewController.firstApplicableViewController()
		} else {
			return self
		}
	}
	
	func topMostViewController() -> UIViewController? {
		let keyWindow: UIWindow = UIApplication.shared.keyWindow!
		let rootViewController = keyWindow.rootViewController
		return rootViewController!.firstApplicableViewController()
	}
}
