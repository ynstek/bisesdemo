//
//  UIImageExtension.swift
//  BisesDemo
//
//  Created by Yunus TEK on 28.01.2019.
//  Copyright © 2019 Yunus TEK. All rights reserved.
//

import UIKit

extension UIImage {	

	// MARK: - UIImage+Resize
	
	public func compressTo(_ expectedSizeInMb:Double) -> Data? {
		let sizeInBytes = expectedSizeInMb * 1024 * 1024
		var needCompress:Bool = true
		var imgData:Data?
		var compressingValue:CGFloat = 1.0
		while (needCompress && compressingValue > 0.0) {
			if let data: Data = self.jpegData(compressionQuality: compressingValue) {
				if Double(data.count) < sizeInBytes {
					needCompress = false
					imgData = data
				} else {
					compressingValue -= 0.1
				}
			}
		}
		
		if let data = imgData {
			if (Double(data.count) < sizeInBytes) {
				return data
			}
		}
		return nil
	}
}
