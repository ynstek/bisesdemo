//
//  GradientColor.swift
//  BisesDemo
//
//  Created by Yunus TEK on 27.01.2019.
//  Copyright © 2019 Yunus TEK. All rights reserved.
//

import UIKit

enum PointPozition: Int {
	case yukariAsagi = 0, solSag
}

extension UIView {
	func setGradientBackground(one: UIColor = UIColor.softBlue, two: UIColor = UIColor.blue, pointPozition: PointPozition = .yukariAsagi) {
		let gradientLayer = CAGradientLayer()
		gradientLayer.frame = self.bounds
		gradientLayer.colors = [one.cgColor, two.cgColor]
		
		
		gradientLayer.locations = [0.0, 1.0]
		
		switch pointPozition {
		case .yukariAsagi:
			// Yukaridan asagiya
			gradientLayer.startPoint = CGPoint(x: 0.5, y: 0.0)
			gradientLayer.endPoint = CGPoint(x: 0.5, y: 1.0)
			break
		case .solSag:
			// Soldan Saga
			gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
			gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
			break
		}

		// 0,0   .5,0   1,0
		// 0,.5  .5,.5  1,.5
		// 0,1   .5,1   1,1
		
		layer.insertSublayer(gradientLayer, at: 0)
	}
}
