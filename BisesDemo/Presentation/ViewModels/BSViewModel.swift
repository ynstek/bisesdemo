//
//  BSViewModel.swift
//  BisesDemo
//
//  Created by Yunus TEK on 20.01.2019.
//  Copyright © 2019 Yunus TEK. All rights reserved.
//

import UIKit

open class BSViewModel {
	
	func loading(_ isLoading: Bool) {
		if isLoading {
			BisesIndicatorPresenter().present()
			UIActivityIndicatorView().startAnimating()
			UIApplication.shared.isNetworkActivityIndicatorVisible = true
		} else {
			BisesIndicatorPresenter().hide()
			UIActivityIndicatorView().stopAnimating()
			UIApplication.shared.isNetworkActivityIndicatorVisible = false
		}
	}
	
}
