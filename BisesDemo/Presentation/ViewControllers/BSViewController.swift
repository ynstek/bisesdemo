//
//  BSViewController.swift
//  BisesDemo
//
//  Created by Yunus TEK on 23.12.2018.
//  Copyright © 2018 Yunus TEK. All rights reserved.
//

import AVKit
import UIKit

open class BSViewController: UIViewController {

	// MARK: Variables
	var logoImageVisible = false
	
	override open func viewDidLoad() {
        // Super
        super.viewDidLoad()
		
		// Prepare UI
		self.prepareUI()
    }
	
	// MARK: Prepare UI {
	override open func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		self.setTabBarItemColor()
	}
	
	// MARK: Prepare UI
	fileprivate func prepareUI() {
		// Transparent Tabbar
		if let tabbarController = self.tabBarController {
			tabbarController.tabBar.backgroundColor = UIColor.clear
			tabbarController.tabBar.isTranslucent = true
		}
		
		// Add logo on navigationItem
		if (self.logoImageVisible) {
			let frame = CGRect(x: 0, y: 0, width: 150, height: 35)
			let image = UIImage(named: "NavigationLogo")
			let imageView = UIImageView(frame: frame)
			imageView.image = image
			imageView.contentMode = .scaleAspectFit
			
			let logoView = UIView(frame: frame)
			logoView.addSubview(imageView)
			
			self.navigationItem.titleView = logoView;
		}
	}
	
	func setTabBarItemColor(selected: UIColor = UIColor.blue, unSelected: UIColor = UIColor.lightGray) {
		// Transparent Tabbar
		if let tabbarController = self.tabBarController {
			tabbarController.tabBar.tintColor = selected
			// Image Tint Color
			if #available(iOS 10.0, *) {
				tabbarController.tabBar.unselectedItemTintColor = unSelected
			} else {
				// Fallback on earlier versions
			}
		}
	}
	
    func openAudioSilentMode() {
        if #available(iOS 10.0, *) {
            do {
                // Play Audio when device in silent mode
                try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default, options: .defaultToSpeaker)
            }
            catch { }
        }
    }
    
	override open func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Super
        super.prepare(for: segue, sender: sender)
        self.backButton()
    }
    
    func present(storyboardName: String, viewControllerId: String, animated: Bool = true, completion: (() -> Void)? = nil) {
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: viewControllerId)
        self.present(controller, animated: animated, completion: completion)
    }
	
	func present(storyboardName: String, animated: Bool = true, completion: (() -> Void)? = nil) {
		let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
		let controller = storyboard.instantiateInitialViewController()
		self.present(controller!, animated: animated, completion: completion)
	}
}
