//
//  BisesIndicatorPresenter.swift
//  BisesDemo
//
//  Created by Yunus TEK on 2.02.2019.
//  Copyright © 2019 Yunus TEK. All rights reserved.
//

import UIKit
import Lottie

final class BisesIndicatorPresenter: NSObject {
	
	// MARK: - Constants
	
	private let animationViewJson = "3165-loader"
	private let animationViewTag = 1000
	private let transformScale: CGFloat = 0.2
	private let animationSpeed: CGFloat = 2

	// MARK: Properties
	
	fileprivate var animationView: LOTAnimationView?
	fileprivate var isHidden = false

	// MARK: Presenter Methods
	
	func present() {
		presentInternal(in: nil)
	}
	
	func hide() {
		hideInternal(in: nil)
	}
	
	// MARK: Helper Methods
	
	private func hideInternal(in viewController: UIViewController?) {
		
//		DispatchQueue.main.async { [weak self] in
//			guard let self = self else { return }
		
			// Configure animation view
			var animationViewController = viewController
			if animationViewController == nil {
				animationViewController = UIApplication.shared.topMostViewController()
			}
			
			// Pause animation
			self.animationView?.pause()
			
			// Remove animation view
			if let animationView = animationViewController?.view.viewWithTag(self.animationViewTag) {
				animationView.removeFromSuperview()
				
				// Set visibility
				self.isHidden = false
			}
//		}
		
		// End ignore interaction event
		if UIApplication.shared.isIgnoringInteractionEvents {
			UIApplication.shared.endIgnoringInteractionEvents()
		}
	}
	
	private func presentInternal(in viewController: UIViewController?) {
		// Start ignore interaction event
		if !UIApplication.shared.isIgnoringInteractionEvents {
			UIApplication.shared.beginIgnoringInteractionEvents()
		}
		
		if self.isHidden {
			return
		}
		
		// Configure animation view
		var animationViewController = viewController
		if animationViewController == nil {
			animationViewController = UIApplication.shared.topMostViewController()
		}
		
		self.animationView = LOTAnimationView(name: self.animationViewJson)
		self.animationView?.contentMode = .scaleAspectFit
		self.animationView?.tag = self.animationViewTag
		self.animationView?.loopAnimation = true
		self.animationView?.frame = animationViewController?.view.bounds ?? .zero
		self.animationView?.transform = CGAffineTransform(scaleX: self.transformScale, y: self.transformScale)
		self.animationView?.play()
		self.animationView?.animationSpeed = animationSpeed
		
		// Set visibility
		self.isHidden = true
		
		// Add subview
		animationViewController?.view.addSubview(self.animationView!)
		animationViewController?.view.bringSubviewToFront(self.animationView!)
	}
	
}
