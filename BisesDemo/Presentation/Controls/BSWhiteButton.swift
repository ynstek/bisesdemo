//
//  BSWhiteButton.swift
//  BisesDemo
//
//  Created by Yunus TEK on 20.01.2019.
//  Copyright © 2019 Yunus TEK. All rights reserved.
//

import UIKit

open class BSWhiteButton: BSButton {
	
	override func initialize() {
		self.backgroundColor = UIColor.white
		self.tintColor = UIColor("#42A5F5")
	}
}
