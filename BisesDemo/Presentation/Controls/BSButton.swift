//
//  BSButton.swift
//  BisesDemo
//
//  Created by Yunus TEK on 20.01.2019.
//  Copyright © 2019 Yunus TEK. All rights reserved.
//

import UIKit

open class BSButton: UIButton {
	
	public required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		self.initialize()
	}
	
	public override init(frame: CGRect) {
		super.init(frame: frame)
		self.initialize()
	}
	
	func initialize() {
		self.backgroundColor = UIColor.blue
		self.tintColor = UIColor.white
	}
}
