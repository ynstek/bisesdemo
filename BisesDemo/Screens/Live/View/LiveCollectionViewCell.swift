//
//  LiveCollectionViewCell.swift
//  BisesDemo
//
//  Created by Yunus TEK on 20.01.2019.
//  Copyright © 2019 Yunus TEK. All rights reserved.
//

import UIKit

class LiveCollectionViewCell: UICollectionViewCell {
	@IBOutlet var dateTitleLabel: UILabel!
	@IBOutlet var dateValueLabel: UILabel!
	@IBOutlet var awardTitleLabel: UILabel!
	@IBOutlet var awardValueLabel: UILabel!
	@IBOutlet var view: UIView!
	
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
	}
}
