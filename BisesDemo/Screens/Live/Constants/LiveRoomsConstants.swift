//
//  LiveRoomsConstants.swift
//  BisesDemo
//
//  Created by Yunus TEK on 18.01.2019.
//  Copyright © 2019 Yunus TEK. All rights reserved.
//

import UIKit
import AgoraRtcEngineKit

struct LiveRoomsConstants {
	static let AppId: String = "4996f746a48a48878509e74d1864e720"
	static let ClientRole: AgoraClientRole = .audience
	static let RoomName: String = "bises"
	static let VideoProfile: CGSize = AgoraVideoDimension160x120
}

