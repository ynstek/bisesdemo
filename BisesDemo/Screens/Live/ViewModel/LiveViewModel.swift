//
//  LiveViewModel.swift
//  BisesDemo
//
//  Created by Yunus TEK on 20.01.2019.
//  Copyright © 2019 Yunus TEK. All rights reserved.
//

import Foundation

final public class LiveViewModel: BSViewModel {

	// MARK: Properties
	public var dateTitle: String?
	public var dateValue: String?
	public var awardTitle: String?
	public var awardValue: String?
	
	var lists: [LiveViewModel] = []
	
	// MARK: - Init Methods
	
	override init() {
		super.init()
		self.generateDummyData()
	}
	
	init(dateTitle: String, dateValue: String, awardTitle: String, awardValue: String) {
		self.dateTitle = dateTitle
		self.dateValue = dateValue
		self.awardTitle = awardTitle
		self.awardValue = awardValue
	}
	
	// MARK: Generate Dummy
	func generateDummyData() {
		lists.append(LiveViewModel(dateTitle: "Bugün ve her gün 20:00", dateValue: "Günün\nYarışması", awardTitle: "Ödül", awardValue: "2.000 TL"))
		lists.append(LiveViewModel(dateTitle: "Bu Pazar 20:00", dateValue: "Haftanın\nFinali", awardTitle: "Ödül", awardValue: "6.000 ₺"))
		lists.append(LiveViewModel(dateTitle: "30 Aralık Pazar 20:00", dateValue: "Ay Sonu\nBüyük Final", awardTitle: "Büyük Ödül", awardValue: "12.000 ₺"))
	}
}
