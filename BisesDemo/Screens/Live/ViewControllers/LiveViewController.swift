//
//  LiveViewController.swift
//  BisesDemo
//
//  Created by Yunus TEK on 29.12.2018.
//  Copyright © 2018 Yunus TEK. All rights reserved.
//

import Firebase
import FirebaseStorage
import UIKit

final class LiveViewController: BSViewController {
		
	// MARK: Outlets
	
	@IBOutlet var liveCollectionView: UICollectionView!
	@IBOutlet var ProfileImage: UIImageView!
	
	// MARK: Variables
	
	private let LiveCellIdentifier = "liveCell"
	var viewModel: LiveViewModel!

	// MARK: View Lifecycle

    override func viewDidLoad() {
		self.logoImageVisible = true
		super.viewDidLoad()
		
		// Prepare UI
		self.prepareUI()
	}
	
	// MARK: Prepare UI
	
	fileprivate func prepareUI() {
		self.liveCollectionView.delegate = self
		self.liveCollectionView.dataSource = self
		self.viewModel = LiveViewModel()
		
		self.downloadProfileImage()
		self.registerCollectionViewNibs()		
	}
	
	// MARK: Actions
	
	@IBAction func goLiveButton(_ sender: UIButton) {
		performSegue(withIdentifier: "mainToLive", sender: nil)
	}
	
	@IBAction func signOuth(_ sender: UIButton) {
		self.showAlert("Emin misin?", bodyMessage: "Çıkış yapmak istediğine emin misin?", yes: {
			let firebaseAuth = Auth.auth()
			do {
				try firebaseAuth.signOut()
				// Login
				self.present(storyboardName: StoryboardNameConstants.Login)
			} catch let signOutError as NSError {
				print ("Error signing out: %@", signOutError)
				self.showAlert("Hata", bodyMessage: signOutError.localizedDescription)
			}
		}, no: nil)
	}
}

extension LiveViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		return CGSize(width: self.view.frame.width - 16, height: 120)
	}
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return self.viewModel.lists.count
	}
	
	func numberOfSections(in collectionView: UICollectionView) -> Int {
		return 1
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: LiveCellIdentifier, for: indexPath) as! LiveCollectionViewCell
		
		let data = self.viewModel.lists[indexPath.item]
		cell.dateTitleLabel.text = data.dateTitle
		cell.dateValueLabel.text = data.dateValue
		cell.awardTitleLabel.text = data.awardTitle
		cell.awardValueLabel.text = data.awardValue
		cell.view.setGradientBackground(pointPozition: .solSag)
		
		return cell
	}
	
	// MARK: Helper Methods
	func registerCollectionViewNibs() {
		let nib = UINib(nibName: "LiveCollectionViewCell", bundle: nil)
		self.liveCollectionView.register(nib, forCellWithReuseIdentifier: LiveCellIdentifier)
	}
}

extension LiveViewController {
	
	func downloadProfileImage() {
		let storage = Storage.storage()
		let storageRef = storage.reference()

		if let uid = Auth.auth().currentUser?.uid {
			let islandRef = storageRef.child("images/profile/" + uid + ".jpg")
			
			islandRef.getData(maxSize: 1 * 1024 * 1024) { data, error in
				if error != nil {
					// Uh-oh, an error occurred!
					debugPrint(error!.localizedDescription)
				} else {
					let image = UIImage(data: data!)
					self.ProfileImage.image = image
				}
			}
		}
	}
	
}

