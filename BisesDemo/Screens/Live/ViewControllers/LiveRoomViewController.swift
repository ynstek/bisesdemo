//
//  LiveRoomViewController.swift
//  BisesDemo
//
//  Created by Yunus TEK on 13.01.2019.
//  Copyright © 2019 Yunus TEK. All rights reserved.
//

import AgoraRtcEngineKit
import Lottie
import UIKit

class LiveRoomViewController: BSViewController {
	
	// MARK: Constants
	
	private let animationViewJson = "musicly-background"
	private let animationViewTag = 1001
	private let transformScale: CGFloat = 1
	private let animationSpeed: CGFloat = 0.65
	
	// MARK: Properties
	
	fileprivate var animationView: LOTAnimationView?
	
	// MARK: Outlets
	
	@IBOutlet weak var remoteContainerView: UIView!
	
	// MARK: Variables
	
	fileprivate let viewLayouter = VideoViewLayouter()
	var rtcEngine: AgoraRtcEngineKit!
	fileprivate var isBroadcaster: Bool {
		return LiveRoomsConstants.ClientRole == .broadcaster
	}
	fileprivate var videoSessions = [VideoSession]() {
		didSet {
			guard remoteContainerView != nil else {
				return
			}
			updateInterface(withAnimation: true)
		}
	}
	fileprivate var fullSession: VideoSession? {
		didSet {
			if fullSession != oldValue && remoteContainerView != nil {
				updateInterface(withAnimation: true)
			}
		}
	}
	
	// MARK: View Lifecycle
	
    override func viewDidLoad() {
        super.viewDidLoad()

		// Prepare UI
		self.prepareUI()
    }
	
	// MARK: Prepare UI
	
	private func prepareUI() {
		self.presentBackgroundAnimation()
		self.loadAgoraKit()
	}
	
	override var preferredStatusBarStyle: UIStatusBarStyle {
		return .lightContent
	}
	
	// MARK: Actions
	
	@IBAction func leaveButton(_ sender: UIButton) {
		leaveChannel()
	}
}

private extension LiveRoomViewController {
	
	func leaveChannel() {
		setIdleTimerActive(true)
		
		rtcEngine.setupLocalVideo(nil)
		rtcEngine.leaveChannel(nil)
		if isBroadcaster {
			rtcEngine.stopPreview()
		}
		
		for session in videoSessions {
			session.hostingView.removeFromSuperview()
		}
		videoSessions.removeAll()
		
		self.showAlert("Canlı Yayından Çık?", bodyMessage: "Canlı yayından çıkmak istediğinize emin misiniz?", yes: {
			self.dismiss(animated: true, completion: nil)
			//			_ = navigationController?.popViewController(animated: true)
		})
		
	}
	
	func setIdleTimerActive(_ active: Bool) {
		UIApplication.shared.isIdleTimerDisabled = !active
	}
}

private extension LiveRoomViewController {
	func updateInterface(withAnimation animation: Bool) {
		if animation {
			UIView.animate(withDuration: 0.3) {
				self.updateInterface()
				self.view.layoutIfNeeded()
			}
			
		} else {
			updateInterface()
		}
	}
	
	func updateInterface() {
		var displaySessions = videoSessions
		if !isBroadcaster && !displaySessions.isEmpty {
			displaySessions.removeFirst()
		}
		viewLayouter.layout(sessions: displaySessions, fullSession: fullSession, inContainer: remoteContainerView)
		setStreamType(forSessions: displaySessions, fullSession: fullSession)
	}
	
	func setStreamType(forSessions sessions: [VideoSession], fullSession: VideoSession?) {
		if let fullSession = fullSession {
			for session in sessions {
				rtcEngine.setRemoteVideoStream(UInt(session.uid), type: (session == fullSession ? .high : .low))
			}
		} else {
			for session in sessions {
				rtcEngine.setRemoteVideoStream(UInt(session.uid), type: .high)
			}
		}
	}
	
	func addLocalSession() {
		let localSession = VideoSession.localSession()
		videoSessions.append(localSession)
		rtcEngine.setupLocalVideo(localSession.canvas)
	}
	
	func fetchSession(ofUid uid: Int64) -> VideoSession? {
		for session in videoSessions {
			if session.uid == uid {
				return session
			}
		}
		
		return nil
	}
	
	func videoSession(ofUid uid: Int64) -> VideoSession {
		if let fetchedSession = fetchSession(ofUid: uid) {
			return fetchedSession
		} else {
			let newSession = VideoSession(uid: uid)
			videoSessions.append(newSession)
			return newSession
		}
	}
}

//MARK: - Agora Media SDK
private extension LiveRoomViewController {
	func loadAgoraKit() {
		rtcEngine = AgoraRtcEngineKit.sharedEngine(withAppId: LiveRoomsConstants.AppId, delegate: self)
		rtcEngine.setChannelProfile(.liveBroadcasting)
		rtcEngine.enableDualStreamMode(true)
		rtcEngine.enableVideo()
		rtcEngine.setVideoEncoderConfiguration(AgoraVideoEncoderConfiguration(size: LiveRoomsConstants.VideoProfile,
																			  frameRate: .fps15,
																			  bitrate: AgoraVideoBitrateStandard,
																			  orientationMode: .adaptative))
		rtcEngine.setClientRole(LiveRoomsConstants.ClientRole)
		
		if isBroadcaster {
			rtcEngine.startPreview()
		}
		
		addLocalSession()
		
		let code = rtcEngine.joinChannel(byToken: nil, channelId: LiveRoomsConstants.RoomName, info: nil, uid: 0, joinSuccess: nil)
		if code == 0 {
			setIdleTimerActive(false)
			rtcEngine.setEnableSpeakerphone(true)
		} else {
			DispatchQueue.main.async(execute: {
				self.showAlert("Bağlantı kurulamadı!", bodyMessage: "Hata kodu: \(code)")
			})
		}
	}
}

extension LiveRoomViewController: AgoraRtcEngineDelegate {
	func rtcEngine(_ engine: AgoraRtcEngineKit, didJoinedOfUid uid: UInt, elapsed: Int) {
		let userSession = videoSession(ofUid: Int64(uid))
		rtcEngine.setupRemoteVideo(userSession.canvas)
	}
	
	func rtcEngine(_ engine: AgoraRtcEngineKit, firstLocalVideoFrameWith size: CGSize, elapsed: Int) {
		if let _ = videoSessions.first {
			updateInterface(withAnimation: false)
		}
	}
	
	func rtcEngine(_ engine: AgoraRtcEngineKit, didOfflineOfUid uid: UInt, reason: AgoraUserOfflineReason) {
		var indexToDelete: Int?
		for (index, session) in videoSessions.enumerated() {
			if session.uid == Int64(uid) {
				indexToDelete = index
			}
		}
		
		if let indexToDelete = indexToDelete {
			let deletedSession = videoSessions.remove(at: indexToDelete)
			deletedSession.hostingView.removeFromSuperview()
			
			if deletedSession == fullSession {
				fullSession = nil
			}
		}
	}
}


// MARK: Lottie

extension LiveRoomViewController {
	
	private func presentBackgroundAnimation() {
		// Configure animation view
		let animationViewController = self
		
		self.animationView = LOTAnimationView(name: self.animationViewJson)
		self.animationView?.contentMode = .scaleAspectFill
		self.animationView?.loopAnimation = true
		self.animationView?.tag = self.animationViewTag
		self.animationView?.frame = animationViewController.view.bounds
		self.animationView?.transform = CGAffineTransform(scaleX: self.transformScale, y: self.transformScale)
		self.animationView?.play(fromProgress: 0.05,
								 toProgress: 1,
								 withCompletion: nil
		)
		self.animationView?.animationSpeed = animationSpeed
		
		// Add subview
 		animationViewController.view.insertSubview(self.animationView!, at: 0)
	}
	
	private func hideBackgroundAnimation() {
		// Configure animation view
		let animationViewController = self
		
		// Pause animation
		self.animationView?.pause()
		
		// Remove animation view
		if let animationView = animationViewController.view.viewWithTag(self.animationViewTag) {
			animationView.removeFromSuperview()
		}
	}
	
}
