//
//  SplashViewController.swift
//  BisesDemo
//
//  Created by Yunus TEK on 29.12.2018.
//  Copyright © 2018 Yunus TEK. All rights reserved.
//

import Firebase
import UIKit

class SplashViewController: BSViewController {

	// MARK: - View Lifestyle
	
    override func viewDidLoad() {
        super.viewDidLoad()

		// Prepare UI
		self.prepareUI()
    }
	
	func prepareUI() {
		self.view.setGradientBackground()
	}
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
		
		//
		self.isUserExits { (status) in
			if status {
				self.present(storyboardName: StoryboardNameConstants.Main)
				
			} else {
				self.present(storyboardName: StoryboardNameConstants.Login)
			}
		}
	}
	
	// MARK: Helper
	
	func isUserExits(status: @escaping (Bool) -> () ) {
		if let user = Auth.auth().currentUser {
			let db = Firestore.firestore()
			let docRef = db.collection("User").document(user.uid)
			
			docRef.getDocument { (document, error) in
				if let document = document, document.exists {
					if let _ = document.data()?.index(forKey: "username") {
						status(true)
					}
				}
				
				status(false)
			}
		} else {
			status(false)
		}
	}
}
