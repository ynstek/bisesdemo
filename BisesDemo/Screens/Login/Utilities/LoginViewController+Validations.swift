//
//  LoginViewController+Validations.swift
//  BisesDemo
//
//  Created by Yunus TEK on 26.01.2019.
//  Copyright © 2019 Yunus TEK. All rights reserved.
//

import Foundation

extension LoginViewController {
	func isValidPhone(_ value: String, _ error: Bool = true) -> Bool {
		// With +90
		let PHONE_REGEX = "^((\\+)|(00))[0-9]{6,14}$"
		let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
		var result =  phoneTest.evaluate(with: value)
		
		if value.count != 13 {
			result = false
		}
		
		if Int(value) == 0 {
			result = false
		}
		
		if result == false && error {
			self.showAlert("Uyarı", bodyMessage: "Başında sıfır olmadan 10 karakterli ve geçerli bir cep telefonu numarası girmelisin.")
		}
		
		return result
	}
}
