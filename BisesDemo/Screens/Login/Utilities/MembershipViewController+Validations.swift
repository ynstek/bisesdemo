//
//  MembershipViewController+Validations.swift
//  BisesDemo
//
//  Created by Yunus TEK on 26.01.2019.
//  Copyright © 2019 Yunus TEK. All rights reserved.
//

import Foundation

extension MembershipViewController {
	func isValidUserName(_ error: Bool = true) -> Bool {
		guard let username = self.usernameTextField.text else { return false }
		
		var result = true
		var message = ""
		
		if username.count < 3 {
			message = "Kullanıcı adınız en az 3 karakterli olmalı."
			result = false
		}
		
		if result == false && error {
			self.showAlert("Uyarı", bodyMessage: message)
		}
		
		return result
	}
}
