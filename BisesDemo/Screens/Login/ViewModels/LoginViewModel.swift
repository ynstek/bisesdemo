//
//  LoginViewModel.swift
//  BisesDemo
//
//  Created by Yunus TEK on 26.01.2019.
//  Copyright © 2019 Yunus TEK. All rights reserved.
//

import Firebase
import FirebaseFirestore

final public class LoginViewModel: BSViewModel {
	
	// MARK: Properties
	private var loginService: LoginService!
	
	// MARK: Init Methods
	override init() {
		// Initialize service
		self.loginService = LoginService()
		super.init()
	}
	
	// MARK: Service Calls
	
	func callVerifyPhoneNumber(phoneNumber: String, uiDelegate: AuthUIDelegate? = nil, completion: @escaping (String?, Error?) -> ()) {
		self.loading(true)
		// Change language code to french.
		Auth.auth().languageCode = "tr";
		self.loginService.callVerifyPhoneNumber(phoneNumber: phoneNumber, uiDelegate: uiDelegate) { [weak self] (verificationID, error) in
			self?.loading(false)
			completion(verificationID, error)
		}
	}
	
	func savePhoto(imageData: Data?, imageExtensionUrl: URL?, complete: @escaping (Error?) -> ()) {
		self.loading(true)
		self.loginService.saveProfilePhoto(imageData: imageData, imageExtensionUrl: imageExtensionUrl) { [weak self] (error) in
			self?.loading(false)
			complete(error)
		}
	}
	
	func signIn(code: String, verificationID: String, complete: @escaping (AuthDataResult?, Error?) -> ()) {
		self.loading(true)
		self.loginService.signIn(code: code, verificationID: verificationID) { [weak self] (authResult, error) in
			self?.loading(false)
			complete(authResult,error)
		}
	}
	
	func getDocument(collection: String, document: String, complete: @escaping (DocumentSnapshot?, Error?) -> ()) {
		self.loading(true)
		self.loginService.getDocument(collection: collection, document: document) { [weak self] (documentSnapshot, error) in
			self?.loading(false)
			complete(documentSnapshot,error)
		}
	}
	
	func setData(data: [String: Any], collection: String, document: String, complete: @escaping (Error?) -> ()) {
		self.loading(true)
		self.loginService.setData(data: data, collection: collection, document: document) { [weak self] (error) in
			self?.loading(false)
			complete(error)
		}
	}
	
}
