//
//  LoginService.swift
//  BisesDemo
//
//  Created by Yunus TEK on 26.01.2019.
//  Copyright © 2019 Yunus TEK. All rights reserved.
//

import Firebase
import FirebaseFirestore
import UIKit

final public class LoginService: BSService {

	// Properties
	let dataBase = Firestore.firestore()
	
	func callVerifyPhoneNumber(phoneNumber: String, uiDelegate: AuthUIDelegate? = nil, completion: @escaping (String?, Error?) -> ()) {
		PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: uiDelegate) { verificationID, error in
			completion(verificationID, error)
		}
	}
	
	func saveProfilePhoto(imageData: Data?, imageExtensionUrl: URL?, complete: @escaping (Error?) -> ()) {
		guard let data = imageData else {
			complete(nil)
			return
		}
		
		let storage = Storage.storage()
		let storageRef = storage.reference()
		
		if let uid = Auth.auth().currentUser?.uid {
			var fileType = ""
			
			if let url = imageExtensionUrl, url.absoluteString.hasSuffix("JPG") || url.absoluteString.hasSuffix("JPEG") {
				fileType = "jpg"
			} else {
				fileType = "png"
			}
			
			let imageRef = storageRef.child("images/profile/" + uid + ".jpg")
			let metadata = StorageMetadata()
			metadata.contentType = "image/jpg" + fileType
			
			let uploadTask = imageRef.putData(data, metadata: metadata)
			uploadTask.observe(.resume) { snapshot in
				
			}
			
			uploadTask.observe(.pause) { snapshot in
				
			}
			
			uploadTask.observe(.progress) { snapshot in
				// Upload reported progress
				let percentComplete = 100.0 * Double(snapshot.progress!.completedUnitCount)
					/ Double(snapshot.progress!.totalUnitCount)
				print("percentComplete", percentComplete)
			}
			
			uploadTask.observe(.success) { snapshot in
				complete(snapshot.error)
			}
			
			uploadTask.observe(.failure) { snapshot in
				complete(snapshot.error)
				if let error = snapshot.error as NSError? {
					switch (StorageErrorCode(rawValue: error.code)!) {
					case .objectNotFound:
						// File doesn't exist
						print("File doesn't exist", error)
						break
					case .unauthorized:
						// User doesn't have permission to access file
						print("User doesn't have permission to access file", error)
						break
					case .cancelled:
						// User canceled the upload
						print("User canceled the upload", error)
						break
					case .unknown:
						// Unknown error occurred, inspect the server response
						print("Unknown error occurred, inspect the server response", error)
						break
					default:
						// A separate error occurred. This is a good place to retry the upload.
						print("A separate error occurred. This is a good place to retry the upload.", error)
						break
					}
				}
			}
			
		}
	}
	
	func signIn(code: String, verificationID: String, complete: @escaping (AuthDataResult?, Error?) -> ()) {
		let credential = PhoneAuthProvider.provider().credential(
			withVerificationID: verificationID,
			verificationCode: code)
		
		Auth.auth().signInAndRetrieveData(with: credential) { (authResult, error) in
			complete(authResult,error)
		}
	}
	
	func getDocument(collection: String, document: String, complete: @escaping (DocumentSnapshot?, Error?) -> ()) {
		let docRef = dataBase.collection(collection).document(document)
		
		docRef.getDocument { (documentSnapshot, error) in
			complete(documentSnapshot, error)
		}
	}
	
	func setData(data: [String: Any], collection: String, document: String, complete: @escaping (Error?) -> ()) {
		let userCol = dataBase.collection(collection)
		
		userCol.document(document).setData(data) { error in
			complete(error)
		}
	}
	
}
