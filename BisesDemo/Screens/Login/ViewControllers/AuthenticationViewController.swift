//
//  AuthenticationViewController.swift
//  BisesDemo
//
//  Created by Yunus TEK on 30.12.2018.
//  Copyright © 2018 Yunus TEK. All rights reserved.
//

import Firebase
import FirebaseFirestore
import UIKit

class AuthenticationViewController: BSViewController {

	// MARK: Outlets

	@IBOutlet var phoneLabel: UILabel! {
		didSet{
			self.phoneLabel.text = self.phoneNumber
		}
	}
	@IBOutlet var codeTextView: UITextField!

	// MARK: Properties

	var phoneNumber = ""
	var user: User?
	var verificationID: String = ""
	var viewModel: LoginViewModel!
	
	// MARK: View Lifecycle

	override func viewDidLoad() {
        super.viewDidLoad()

		// Set Delegates
		self.codeTextView.delegate = self
		
		// Prepare UI
		self.prepareUI()
	}
	
	func prepareUI() {
		self.view.setGradientBackground()
		self.viewModel = LoginViewModel()
	}
	
	// MARK: Actions
	
	@IBAction func approveButton(_ sender: BSWhiteButton) {
		guard let code = self.codeTextView.text else { return }
		
		if self.isValidCode(code) {
			self.viewModel.signIn(code: code, verificationID: self.verificationID) { (authResult, error) in
				if let error = error {
					self.showAlert("Hata", bodyMessage: error.localizedDescription)
					return
				}
				self.user = authResult?.user
				self.hasUser()
			}
		}
	}
	
	func hasUser() {
		
		self.viewModel.getDocument(collection: "User", document: self.user!.uid) { [weak self] (document, error) in
			guard let self = self else {return}
			
			if let document = document, document.exists {
				if let _ = document.data()?.index(forKey: "username") {
					// Show Home Page
					self.present(storyboardName: StoryboardNameConstants.Main)
				} else {
					// Show Membership
					self.performSegue(withIdentifier: "membership", sender: nil)
				}
				
			} else {
				
				let uid = Auth.auth().currentUser!.uid
				let userData: [String : Any] = [
					"phone": "+90" + self.phoneNumber
				]
				
				self.viewModel.setData(data: userData, collection: "User", document: uid, complete: { [weak self] (error) in
					// Show Membership
					self?.performSegue(withIdentifier: "membership", sender: nil)
				})
	
			}
			
		}

	}
	
}

// MARK: Text Field Delegates

extension AuthenticationViewController: UITextFieldDelegate {
	func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
		guard textField == codeTextView else { return true }
		
		if textField.text!.count < 6 {
			return true
		} else if textField.text!.count == 6 && string == "" {
			return true
		}
		return false
	}
	
	func isValidCode(_ value: String, _ error: Bool = true) -> Bool {
		let result = value.count != 6 ? false : true
		if result == false && error {
			self.showAlert("Uyarı", bodyMessage: "Lütfen 6 karakterli bir kod giriniz.")
		}
		return result
	}
}
