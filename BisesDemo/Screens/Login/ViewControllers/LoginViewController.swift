//
//  LoginViewController.swift
//  BisesDemo
//
//  Created by Yunus TEK on 23.12.2018.
//  Copyright © 2018 Yunus TEK. All rights reserved.
//

import Firebase
import UIKit

final class LoginViewController: BSViewController {

	// MARK: Outlets

	@IBOutlet var phoneNumberTextView: UITextField!
	@IBOutlet var skipButton: UIButton!
	
	// MARK: Properties
	
	var viewModel: LoginViewModel!
	
	// MARK: View Lifecycle

	override func viewDidLoad() {
        super.viewDidLoad()
		
		// Set Delegates
		self.phoneNumberTextView.delegate = self
		
		// Transparent NavigationBar
		self.presentTransparentNavigationBar()
		
		// Prepare UI
		self.prepareUI()
    }
	
	func prepareUI() {
		self.viewModel = LoginViewModel()
//		self.skipButton.isHidden = true
		self.view.setGradientBackground()
	}
	
	// MARK: Actions
	
	@IBAction func approveButton(_ sender: BSWhiteButton) {
		guard var phoneNumber = self.phoneNumberTextView.text else { return }
		phoneNumber = "+90" + phoneNumber
		
		if self.isValidPhone(phoneNumber) {
			self.viewModel.callVerifyPhoneNumber(phoneNumber: phoneNumber) { [weak self] verificationID, error in
				if let error = error {
					self?.showAlert("Hata", bodyMessage: error.localizedDescription)
					return
				}
				
				self?.performSegue(withIdentifier: "authentication", sender: verificationID)
			}
		}
	}
	
    @IBAction func skipButton(_ sender: UIButton) {
        self.present(storyboardName: StoryboardNameConstants.Main)
    }
	
	// MARK: Segues
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "authentication" {
			let vc = segue.destination as? AuthenticationViewController
			vc?.verificationID = sender as! String
			vc?.phoneNumber = self.phoneNumberTextView.text!
		}
	}
	
}


// MARK: Text Field Delegates

extension LoginViewController: UITextFieldDelegate {
	
	func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
		guard textField == phoneNumberTextView else { return true }
		
		if textField.text!.count < 10 {
			return true
		} else if textField.text!.count == 10 && string == "" {
			return true
		}
		return false
	}
}
