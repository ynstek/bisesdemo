//
//  MembershipViewController.swift
//  BisesDemo
//
//  Created by Yunus TEK on 30.12.2018.
//  Copyright © 2018 Yunus TEK. All rights reserved.
//

import Firebase
import FirebaseFirestore
import UIKit

class MembershipViewController: BSViewController {

	// MARK: Outlets
	
	@IBOutlet var usernameTextField: UITextField!
	@IBOutlet var referanceCode: UITextField!
	@IBOutlet var profileImageView: UIImageView!
	@IBOutlet var editIcon: UIButton!

	// MARK: Properties
	var imageExtensionUrl: URL?
	var imageData: Data?
	var viewModel: LoginViewModel!
	
	// MARK: View Lifecycle

	override func viewDidLoad() {
        super.viewDidLoad()
				
		// Prepare UI
		self.prepareUI()
	}
	
	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		self.profileImageView.setRounded()
		self.profileImageView.setBorderColor()
	}
	
	func prepareUI() {
		self.viewModel = LoginViewModel()
		self.view.setGradientBackground()
		self.removeProfile()
	}
	
	// MARK: Actions

	@IBAction func approveButton(_ sender: UIButton) {
		if self.isValidUserName() {
			let db = Firestore.firestore()
			let userCol = db.collection("User")
			let uid = Auth.auth().currentUser!.uid
			
			let data: [String : Any] = [
				"username": self.usernameTextField.text!,
				"referanceCode": self.referanceCode.text!
			]
			
			self.viewModel.savePhoto(imageData: self.imageData, imageExtensionUrl: self.imageExtensionUrl) { [weak self] error in
				guard let self = self else {return}
				
				guard error == nil else {
					self.showAlert("Uyarı", bodyMessage: "Bir hata oluştu. Lütfen tekrar deneyiniz.\n" + error!.localizedDescription)
					return
				}
				
				userCol.document(uid).updateData(data) {  [weak self] error in
					guard let self = self else {return}

					guard error == nil else {
						self.showAlert("Uyarı", bodyMessage: "Bir hata oluştu. Lütfen tekrar deneyiniz.\n" + error!.localizedDescription)
						return
					}
					self.present(storyboardName: StoryboardNameConstants.Main)
				}
				
			}
		}
    }
	
	@IBAction func photoButton(_ sender: UIButton) {
		self.prepareActionSheetAlert()
	}
}

extension MembershipViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
	
	func removeProfile() {
		self.imageData = nil
		self.imageExtensionUrl = nil
		self.profileImageView.image = UIImage(named: "defaultProfile")
	}
	
	func prepareActionSheetAlert() {
		let optionMenu = UIAlertController(title: nil, message: "Profil Fotoğrafını Güncelle", preferredStyle: .actionSheet)
		
		if imageData != nil {
			let deletePhotoAction = UIAlertAction(title: "Fotoğrafı Sil", style: .destructive) { (alert) in
				self.removeProfile()
			}
			optionMenu.addAction(deletePhotoAction)
		}
		
		let choosePhotoAction = UIAlertAction(title: "Galeriden Seç", style: .default) { (alert) in
			if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
				let imagePicker = UIImagePickerController()
				imagePicker.delegate = self
				imagePicker.sourceType = .photoLibrary
				imagePicker.allowsEditing = true
				self.present(imagePicker, animated: true, completion: nil)
			}
		}
		optionMenu.addAction(choosePhotoAction)
		
		let takePhotoAction = UIAlertAction(title: "Fotoğraf Çek", style: .default) { (alert) in
			if UIImagePickerController.isSourceTypeAvailable(.camera) {
				let imagePicker = UIImagePickerController()
				imagePicker.delegate = self
				imagePicker.sourceType = .camera
				imagePicker.allowsEditing = true
				self.present(imagePicker, animated: true, completion: nil)
			}
		}
		optionMenu.addAction(takePhotoAction)
		
		let cancelAction = UIAlertAction(title: "Vazgeç", style: .cancel) { (alert) in
		}
		optionMenu.addAction(cancelAction)
		
		self.present(optionMenu, animated: true, completion: nil)
	}

	func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
		if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
			if let data = image.compressTo(0.5) {
				dismiss(animated: true) {
					self.imageExtensionUrl = info[UIImagePickerController.InfoKey.referenceURL] as? URL
					self.imageData = data
					
					self.profileImageView.image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
				}
			}
		}
	}	
}
