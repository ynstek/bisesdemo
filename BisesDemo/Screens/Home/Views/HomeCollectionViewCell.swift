//
//  HomeCollectionViewCell.swift
//  BisesDemo
//
//  Created by Yunus TEK on 4.01.2019.
//  Copyright © 2019 Yunus TEK. All rights reserved.
//

import Player
import UIKit

class HomeCollectionViewCell: UICollectionViewCell {

    @IBOutlet var lblName: UILabel!
    @IBOutlet var view: UIView!
    @IBOutlet var videoProgressBar: UIProgressView!

    var myPlayer = Player()
    var videoUrl: URL?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func stopPlayer() {
        self.myPlayer.stop()
        self.myPlayer.willMove(toParent: nil)
        self.myPlayer.view.removeFromSuperview()
        self.myPlayer.removeFromParent()
    }
    
    func generate() {
    
        self.myPlayer.url = videoUrl
        self.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]

        self.myPlayer.playbackLoops = true
        self.myPlayer.playerView.playerBackgroundColor = .black
        self.myPlayer.playerDelegate = self
        self.myPlayer.playbackDelegate = self
        
        self.myPlayer.playbackPausesWhenResigningActive = false
        self.myPlayer.playbackResumesWhenBecameActive = false
        self.myPlayer.playbackResumesWhenEnteringForeground = false
        self.myPlayer.playbackPausesWhenBackgrounded = false

        let tapGestureRecognizer: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTapGestureRecognizer(_:)))
        tapGestureRecognizer.numberOfTapsRequired = 1
        self.myPlayer.view.addGestureRecognizer(tapGestureRecognizer)
        self.myPlayer.playerView.playerFillMode = .resizeAspectFill
		self.view.insertSubview(self.myPlayer.view, at: 0)
        self.myPlayer.didMove(toParent: HomeViewController())
//        self.addChild(self.player)
//        self.myPlayer.playFromBeginning()
    }
    
    // MARK: - UIGestureRecognizer
    @objc func handleTapGestureRecognizer(_ gestureRecognizer: UITapGestureRecognizer) {
        switch (myPlayer.playbackState.rawValue) {
        case PlaybackState.stopped.rawValue:
            myPlayer.playFromBeginning()
            break
        case PlaybackState.paused.rawValue:
            myPlayer.playFromCurrentTime()
            break
        case PlaybackState.playing.rawValue:
            myPlayer.pause()
            break
        case PlaybackState.failed.rawValue:
            myPlayer.pause()
            break
        default:
            myPlayer.pause()
            break
        }
    }
    
}

extension HomeCollectionViewCell: PlayerDelegate {
    
    func playerReady(_ player: Player) {
        print("\(#function) ready")
    }
    
    func playerPlaybackStateDidChange(_ player: Player) {
        print("\(#function) \(player.playbackState.description)")
    }
    
    func playerBufferingStateDidChange(_ player: Player) {
    }
    
    func playerBufferTimeDidChange(_ bufferTime: Double) {
    }
    
    func player(_ player: Player, didFailWithError error: Error?) {
        print("\(#function) error.description")
    }
    
}

// MARK: - PlayerPlaybackDelegate

extension HomeCollectionViewCell: PlayerPlaybackDelegate {
    
    func playerCurrentTimeDidChange(_ player: Player) {
        let fraction = Double(player.currentTime) / Double(player.maximumDuration)
        var animated = true
        if fraction < 0.05 {
            animated = false
        }
        self.videoProgressBar.setProgress(Float(fraction), animated: animated)
    }
    
    func playerPlaybackWillStartFromBeginning(_ player: Player) {
    }
    
    func playerPlaybackDidEnd(_ player: Player) {
    }
    
    func playerPlaybackWillLoop(_ player: Player) {
    }
    
}
