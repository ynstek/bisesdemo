//
//  HomeViewController.swift
//  BisesDemo
//
//  Created by Yunus TEK on 29.12.2018.
//  Copyright © 2018 Yunus TEK. All rights reserved.
//

import Player
import UIKit

final class HomeViewController: BSViewController {

	// MARK: Outlets
    @IBOutlet var homeCollectionView: UICollectionView!
	
	// MARK: Variables
    private let HomeCellIdentifier = "homeCell"
	private var viewModel = HomeViewModel()
    private var displayedCell: HomeCollectionViewCell? = nil
    private var refreshControl = UIRefreshControl()
	
	// MARK: Init
    deinit {
        print("Deinit HomeViewController")
        if let cv = self.homeCollectionView {
            for cell in cv.visibleCells {
                (cell as? HomeCollectionViewCell)?.stopPlayer()
            }
        }
    }
	
	// MARK: View Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
		
		// Transparent NavigationBar
		self.presentTransparentNavigationBar()
	
		// Bind View
		self.bindView()
		
		// PrepareUI
		self.prepareUI()
	
    }

	// MARK: View Will Appear
	
	public override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		print("viewWillAppear HomeViewController")
		self.displayedCell?.myPlayer.playFromCurrentTime()
		self.setTabBarItemColor(selected: UIColor.white)
	}
	
	// MARK: Bind View
	
	private func bindView() {

	}
	
	// MARK: Private UI

    private func prepareUI() {
        self.homeCollectionView.delegate = self
        self.homeCollectionView.dataSource = self
        registerCollectionViewNibs()
		
        self.openAudioSilentMode()
        self.generateRefreshControl()
    }
    
    func generateRefreshControl() {
        self.homeCollectionView.alwaysBounceVertical = true
        
        self.refreshControl.addTarget(self, action: #selector(HomeViewController.refreshHomeCollectionView), for: .valueChanged)
        self.homeCollectionView.addSubview(self.refreshControl)
    }
    
    @objc func refreshHomeCollectionView() {
        print("refresh")
        // Add Dumy Videos
		self.viewModel.videos.removeAll()
		self.viewModel.generateDummyData()
        
        self.homeCollectionView.reloadData()
        self.refreshControl.endRefreshing()
    }
	
	// MARK: Hide Status Bar
	
	override var prefersStatusBarHidden: Bool {
		return true
	}
}


extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
   
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.width, height: self.view.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return self.viewModel.videos.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell : HomeCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: HomeCellIdentifier, for: indexPath) as! HomeCollectionViewCell
        
        let data = self.viewModel.videos[indexPath.item]
        
        cell.lblName.text = data.videoName
        
        if let path = data.path {
            cell.videoUrl = URL(fileURLWithPath: path)
        }
        
        cell.generate()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard cell.isKind(of: HomeCollectionViewCell.self) else {
            return
        }
        let cell = cell as! HomeCollectionViewCell
        
        if cell != displayedCell {
            cell.myPlayer.pause()
            
            if displayedCell != nil {
                displayedCell?.myPlayer.playFromBeginning()
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard cell.isKind(of: HomeCollectionViewCell.self) else {
            return
        }
        let cell = cell as! HomeCollectionViewCell
    
        if displayedCell == nil {
            cell.myPlayer.playFromBeginning()
        }
        
        self.displayedCell = cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        if let cell = collectionView.cellForItem(at: indexPath) as? HomeCollectionViewCell {
//
//        }
    }

    // MARK: Helper Methods
    func registerCollectionViewNibs() {
        let nib = UINib(nibName: "HomeCollectionViewCell", bundle: nil)
        self.homeCollectionView.register(nib, forCellWithReuseIdentifier: HomeCellIdentifier)
    }
}
