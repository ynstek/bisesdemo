//
//  HomeViewModel.swift
//  BisesDemo
//
//  Created by Yunus TEK on 20.01.2019.
//  Copyright © 2019 Yunus TEK. All rights reserved.
//

import UIKit

final class HomeViewModel: BSViewModel {
	
	// MARK: Properties
	
	public var videoName: String?
	public var color: UIColor?
	public var image: UIImage?
	public var path: String?
	
	var videos: [HomeViewModel] = []

	// MARK: - Init Methods
	
	init(videoName: String, color: UIColor, image: UIImage, path: String) {
		self.videoName = videoName
		self.color = color
		self.image = image
		self.path = path
	}
	
	override init() {
		super.init()
		self.generateDummyData()
	}
	
	// MARK: Generate Dummy
	func generateDummyData() {
		for index in 1...15 {
			videos.append(
				HomeViewModel(
					videoName: String(index) + ". Yarşmacı"
					, color: UIColor.yellow
					, image: UIImage(named: "0" + String(Int.random(in: 1...3)))!
					, path: Bundle.main.path(forResource: "video0" + String(Int.random(in: 1...6))
						, ofType:"mp4")!
				)
			)
		}
	}
	
}
