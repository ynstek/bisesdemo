//
//  MainTabBarController.swift
//  BisesDemo
//
//  Created by Yunus TEK on 23.12.2018.
//  Copyright © 2018 Yunus TEK. All rights reserved.
//

import Firebase
import UIKit

class MainTabBarController: UITabBarController {
	
	public var user: [UserModel]?
	
    override func viewDidLoad() {
        super.viewDidLoad()
		getUser()
        prepareUI()
    }

    fileprivate func prepareUI() {
        // Transparet Tabbar
        let tabBar = UITabBar.appearance()
        tabBar.barTintColor = UIColor.clear
        tabBar.backgroundImage = UIImage()
        tabBar.shadowImage = UIImage()
    }
	
	public func getUser(){
		if let currentUser = Auth.auth().currentUser{
			
			let db = Firestore.firestore()
			let docRef = db.collection("User").document(currentUser.uid)
			
			docRef.getDocument { (document, error) in
				if let document = document, document.exists {
					if let user = document.data().flatMap ({
						$0.compactMap({ data in
							return UserModel(dictionary: data)
						})
					}) {
						self.user = user
					}
				}
			}
		}
	}
    
}
