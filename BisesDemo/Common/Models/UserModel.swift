//
//  UserModel.swift
//  BisesDemo
//
//  Created by Yunus TEK on 21.01.2019.
//  Copyright © 2019 Yunus TEK. All rights reserved.
//

import UIKit

public class UserModel: BSModel {

	var phone: String?
	var username: String?
	var referanceCode: String?
	var profilePhotoUrl: String?

	public override init() {
		super.init()
	}
	
	init?(dictionary: (key: String, value: Any)) {
		super.init()
		switch dictionary.key {
		case "phone":
			self.phone = dictionary.value as? String ?? ""
			break
		case "username":
			self.username = dictionary.value as? String ?? ""
			break
		case "referanceCode":
			self.referanceCode = dictionary.value as? String ?? ""
			break
		case "profilePhotoUrl":
			self.profilePhotoUrl = dictionary.value as? String ?? ""
			break
		default:
			break
		}
	}
}
