//
//  StoryboardIdConstants.swift
//  BisesDemo
//
//  Created by Yunus TEK on 29.12.2018.
//  Copyright © 2018 Yunus TEK. All rights reserved.
//

struct StoryboardIdConstants {
    static let Login = "LoginNavigationController"
    static let Main = "MainTabBarController"
}
