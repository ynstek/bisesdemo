//
//  StoryboardNameConstants.swift
//  BisesDemo
//
//  Created by Yunus TEK on 29.12.2018.
//  Copyright © 2018 Yunus TEK. All rights reserved.
//

import UIKit

struct StoryboardNameConstants {
    static let Home = "Home"
    static let Live = "Live"
    static let Login = "Login"
    static let Main = "Main"
    static let Notification = "Notification"
    static let Profile = "Profile"
    static let Search = "Search"
    static let Splash = "Splash"
}
