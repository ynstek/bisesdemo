//
//  Notification+Name.swift
//  BisesDemo
//
//  Created by Yunus TEK on 6.01.2019.
//  Copyright © 2019 Yunus TEK. All rights reserved.
//

import UIKit

extension Notification.Name {
    static let playerCurrentTimeDidChange = Notification.Name("playerCurrentTimeDidChange")
}

