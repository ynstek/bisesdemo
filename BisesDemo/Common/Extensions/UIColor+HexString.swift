//
//  UIColor+HexString.swift
//  BisesDemo
//
//  Created by Yunus TEK on 24.12.2018.
//  Copyright © 2018 Yunus TEK. All rights reserved.
//

import Foundation
import UIKit

// MARK: - Custom Colors

extension UIColor {
	static var lightGray: UIColor {
		return UIColor("#cfd8dc")
	}
	
	static var blue: UIColor {
		return UIColor("#42A5F5")
	}
	
	static var darkBlue: UIColor {
		return UIColor("#5B7EF0")
	}
	
	static var softBlue: UIColor {
		return UIColor("#3BD8F4")
	}
}

extension UIColor {
    // RGB : UIColor.colorWithRedValue(248, greenValue: 248, blueValue: 248, alpha: 1)
    static func colorWithRedValue(_ redValue: CGFloat, _ greenValue: CGFloat, _ blueValue: CGFloat, alpha: CGFloat) -> UIColor {
        return UIColor(red: redValue/255.0, green: greenValue/255.0, blue: blueValue/255.0, alpha: alpha)
    }
    
    // Hex Color Code : UIColor("#")
    convenience init(_ hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}
